<?php

namespace GqlRootTypeGetter\Tests;

use GqlRootTypeGetter\GraphQlRootTypeGetter;
use GraphQL\Type\Definition\Type;
use GraphQlNullableField\Nullable;
use PHPUnit\Framework\TestCase;

class GraphQlRootTypeGetterTest extends TestCase
{
    /**
     * Набор данных для тестирования метода проверки isList
     * 
     * @return array
     */
    public function isListData() {
        return [
            [Type::listOf(Type::string()), true],
            [Type::listOf(Type::nonNull(Type::boolean())), true],
            [Type::nonNull(Type::listOf(Type::int())), true],
            [Type::nonNull(Type::int()), false],
            [Nullable::create(Type::int()), false],
            [Type::int(), false],
        ];
    }

    /**
     * Тестирование метода isList
     *
     * @dataProvider isListData
     * @param Type $testType
     * @param bool $result
     */
    public function testIsList(Type $testType, bool $result)
    {
        $instance = new GraphQlRootTypeGetter();
        $testResult = $instance->isList($testType);

        $this->assertSame($testResult, $result, sprintf("isList result %b is not equals %b", $testResult, $result));
    }

    /**
     * Набор данных для тестирования метода проверки isNotNull
     *
     * @return array
     */
    public function isNotNullData() {
        return [
            [Type::listOf(Type::string()), false],
            [Type::listOf(Type::nonNull(Type::boolean())), true],
            [Type::nonNull(Type::listOf(Type::int())), true],
            [Type::nonNull(Type::int()), true],
            [Type::int(), false],
        ];
    }

    /**
     * Тестирование метода isNotNull
     *
     * @dataProvider isNotNullData
     * @param Type $testType
     * @param bool $result
     */
    public function testIsNotNull(Type $testType, bool $result)
    {
        $instance = new GraphQlRootTypeGetter();
        $testResult = $instance->isNotNull($testType);

        $this->assertSame($testResult, $result, sprintf("isNotNull result %b is not equals %b", $testResult, $result));
    }

    /**
     * Набор данных для тестирования метода getRootType
     *
     * @return array
     */
    public function getRootTypeData() {
        return [
            [Type::listOf(Type::string()), Type::string()],
            [Type::listOf(Type::nonNull(Type::boolean())), Type::boolean()],
            [Type::nonNull(Type::listOf(Type::int())), Type::int()],
            [Type::nonNull(Type::int()), Type::int()],
            [Type::int(), Type::int()],
            [Nullable::create(Type::int()), Type::int()],
            [Type::listOf(Nullable::create(Type::id())), Type::id()],
        ];
    }

    /**
     * Тестирование метода getRootType
     *
     * @dataProvider getRootTypeData
     * @param Type $testType
     * @param Type $result
     */
    public function testGetRootType(Type $testType, Type $result)
    {
        $instance = new GraphQlRootTypeGetter();
        $testResult = $instance->getRootType($testType);

        $this->assertSame($testResult, $result, sprintf("getRootType result %s is not equals %s", $testResult, $result));
    }

    /**
     * Тестирование метода проверки переданного типа на nullable
     *
     * @return array
     */
    public function dataForIsNullable() {
        return [
            [Type::nonNull(Type::id()), false],
            [Type::nonNull(Nullable::create(Type::string())), true],
            [Type::listOf(Type::int()), false],
            [Type::listOf(Type::nonNull(Type::int())), false],
            [Type::listOf(Type::nonNull(Nullable::create(Type::string()))), true],
            [Type::nonNull(Type::listOf(Type::nonNull(Nullable::create(Type::string())))), true]
        ];
    }

    /**
     * Тестирование метода isNullable библиотеки
     *
     * @dataProvider dataForIsNullable
     * @param Type $testType
     * @param bool $result
     */
    public function testIsNullable(Type $testType, bool $result) {
        $instance = new GraphQlRootTypeGetter();
        $testResult = $instance->isNullable($testType);

        $this->assertEquals($result, $testResult);
    }
}
