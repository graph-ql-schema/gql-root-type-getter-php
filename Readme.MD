### Библиотека получения корневых типов GraphQL по переданным данным

Библиотека позволяет получать корневой тип (скалярный) по переданному полю GraphQL

#### Установка

Для установки необходимо добавить в composer.json

```json
{
    "require": {
        "graph-ql-schema/gql-root-type-getter-php": "^v1"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "https://bitbucket.org/graph-ql-schema/gql-root-type-getter-php.git"
        }
    ]
}
```

#### Использование

Для использования необходимо подключить библиотеку:
```php
use GqlRootTypeGetter\GraphQlRootTypeGetter;

$gqlRootTypeGetter = new GraphQlRootTypeGetter();
```

Доступный функционал:
```php
/**
 * Сервис получения корневого типа GraphQL по переданному.
 */
interface GraphQlRootTypeGetterInterface
{
    /**
     * Получение корневого типа поля, которое обернут в NotNull или в List
     * @param Type $multipleType
     * @return Type
     */
    public function getRootType(Type $multipleType): Type;

    /**
     * Проверяет, что переданный тип является NotNull
     * @param Type $multipleType
     * @return bool
     */
    public function isNotNull(Type $multipleType): bool;

    /**
     * Проверяет, что переданный тип является List
     * @param Type $multipleType
     * @return bool
     */
    public function isList(Type $multipleType): bool;

    /**
     * Проверяет, что переданный тип является Nullable типом
     *
     * @param Type $multipleType
     * @return bool
     */
    public function isNullable(Type $multipleType): bool;
}
```