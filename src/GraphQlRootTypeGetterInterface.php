<?php

namespace GqlRootTypeGetter;

use GraphQL\Type\Definition\Type;

/**
 * Сервис получения корневого типа GraphQL по переданному.
 */
interface GraphQlRootTypeGetterInterface
{
    /**
     * Получение корневого типа поля, которое обернут в NotNull или в List
     * @param Type $multipleType
     * @return Type
     */
    public function getRootType(Type $multipleType): Type;

    /**
     * Проверяет, что переданный тип является NotNull
     * @param Type $multipleType
     * @return bool
     */
    public function isNotNull(Type $multipleType): bool;

    /**
     * Проверяет, что переданный тип является List
     * @param Type $multipleType
     * @return bool
     */
    public function isList(Type $multipleType): bool;

    /**
     * Проверяет, что переданный тип является Nullable типом
     *
     * @param Type $multipleType
     * @return bool
     */
    public function isNullable(Type $multipleType): bool;
}