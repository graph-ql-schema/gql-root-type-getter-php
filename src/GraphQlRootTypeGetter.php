<?php

namespace GqlRootTypeGetter;

use GraphQL\Type\Definition\ListOfType;
use GraphQL\Type\Definition\NonNull;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Type\Definition\Type;
use GraphQlNullableField\Nullable;

/**
 * Сервис получения корневого типа GraphQL по переданному.
 */
class GraphQlRootTypeGetter implements GraphQlRootTypeGetterInterface
{
    /**
     * Получение корневого типа поля, которое обернут в NotNull или в List
     * @param Type $multipleType
     * @return Type
     */
    public function getRootType(Type $multipleType): Type
    {
        if ($multipleType instanceof ListOfType) {
            return $this->getRootType($multipleType->getOfType());
        }

        if ($multipleType instanceof NonNull) {
            return $this->getRootType($multipleType->getOfType());
        }

        if ($multipleType instanceof ScalarType) {
            if (Nullable::isNullable($multipleType)) {
                return Nullable::getBaseType($multipleType);
            }
        }

	    return $multipleType;
    }

    /**
     * Проверяет, что переданный тип является NotNull
     * @param Type $multipleType
     * @return bool
     */
    public function isNotNull(Type $multipleType): bool
    {
        if ($multipleType instanceof ListOfType) {
            return $this->isNotNull($multipleType->getOfType());
        }

        return $multipleType instanceof NonNull;
    }

    /**
     * Проверяет, что переданный тип является List
     * @param Type $multipleType
     * @return bool
     */
    public function isList(Type $multipleType): bool
    {
        if ($multipleType instanceof NonNull) {
            return $this->isList($multipleType->getOfType());
        }

        return $multipleType instanceof ListOfType;
    }

    /**
     * Проверяет, что переданный тип является Nullable типом
     *
     * @param Type $multipleType
     * @return bool
     */
    public function isNullable(Type $multipleType): bool
    {
        if ($multipleType instanceof ListOfType) {
            $subType = $multipleType->getOfType();
            if ($subType instanceof ScalarType) {
                return Nullable::isNullable($subType);
            } else {
                return $this->isNullable($subType);
            }
        }

        if ($multipleType instanceof NonNull) {
            $subType = $multipleType->getOfType();
            if ($subType instanceof ScalarType) {
                return Nullable::isNullable($subType);
            } else {
                return $this->isNullable($subType);
            }
        }

        if ($multipleType instanceof ScalarType) {
            return Nullable::isNullable($multipleType);
        }

        return false;
    }
}